#!/bin/bash

# EmberJS + Ruby on Rails + TailwindCSS + auth + Docker + GitLab + linters + Netlify + Heroku
# Run time: 5m 4s

# Variables
APP="scaffolding"
RUBY_VERSION="2.7.1"

# Basics
set -x
set -e
if [[ -d "$APP" ]]; then
  rm -rf ./$APP
fi
mkdir $APP
cd $APP
git init
cp ../templates/.gitignore ./

# Backend
rbenv install -s ${RUBY_VERSION}
rbenv local ${RUBY_VERSION}
gem install rails
rails new backend --skip-bundle --api --skip-git --database=postgresql
pushd backend
cp ../../templates/.gitignore ./
sed -i '' -e '/database: .*/d' config/database.yml
sed -i '' -e '/username: .*/d' config/database.yml
sed -i '' -e '/default:/r ../../templates/backend/config/database.yml_part_01' config/database.yml
bundle add dotenv-rails -g=test,development
bundle add pgreset -g=development
cp ../../templates/.env ./
popd

pushd backend
# TODO: https://stackoverflow.com/questions/54191982/tzinfo-data-issue-when-starting-new-rails-project
bundle remove tzinfo-data
bundle add tzinfo-data
# TODO: https://stackoverflow.com/questions/20599920/how-to-solve-could-not-find-i18n-0-6-5-in-any-of-the-sources-bundlergemnotfo
bundle update i18n
bundle install
popd

# Docker
cp ../templates/docker-compose.yml ./
cp -r ../templates/docker ./
sed -i -e "s/#{RUBY_VERSION}/${RUBY_VERSION}/g" ./docker/backend.Dockerfile
docker-compose build
# TODO: there is some weird race condition / dependency
docker-compose up db &
sleep 30
docker-compose run backend rails db:setup || true
docker-compose run backend rails db:migrate

# UI
mkdir ui
pushd ui
cp ../../templates/ui/.gitignore ./
cp ../../templates/ui/package.json ./
volta install ember-cli
popd

# Common Components
pushd ui
ember addon monorepo-components --yarn --skip-git --skip-npm
popd

# Frontend
pushd ui
ember new frontend --yarn --skip-git --skip-npm --no-welcome
pushd frontend
sed -i '' -e '/devDependencies/r ../../../templates/ui/frontend/package.json' package.json
sed -i '' -e 's/ember serve/ember serve --proxy=http:\/\/localhost:3000/' package.json
cp ../../../templates/ui/frontend/app/templates/application.hbs ./app/templates/
cp ../../../templates/ui/frontend/tsconfig.json ./
sed -i '' -e '/locationType/r ../../../templates/ui/frontend/config/environment.js_part_02' ./config/environment.js
popd
popd

# UI
pushd ui
yarn
popd

# TailwindCSS
pushd ui/frontend
yarn add ember-cli-postcss tailwindcss postcss-import @fullhuman/postcss-purgecss -D
mkdir app/tailwind
npx tailwind init app/tailwind/config.js
sed -i '' -e '/const EmberApp/r ../../../templates/ui/frontend/ember-cli-build.js_part_01' ember-cli-build.js
sed -i '' -e '/Add options here/r ../../../templates/ui/frontend/ember-cli-build.js_part_02' ember-cli-build.js
touch app/styles/components.css
touch app/styles/utilities.css
cp ../../../templates/ui/frontend/app/styles/app.css ./app/styles/ 
popd

# Login form
pushd ui/frontend
ember g route login
cp ../../../templates/ui/frontend/app/templates/login.hbs ./app/templates/
popd
pushd ui/monorepo-components
ember g component login-form -gc -ns
cp ../../../templates/ui/monorepo-components/addon/components/login-form/index.hbs ./addon/components/login-form/
cp ../../../templates/ui/monorepo-components/addon/components/login-form/index.js ./addon/components/login-form/
ember install ember-changeset
yarn remove ember-changeset --dev && yarn add ember-changeset
ember install ember-changeset-validations
yarn remove ember-changeset-validations --dev && yarn add ember-changeset-validations
ember install ember-validated-form
yarn remove ember-validated-form --dev && yarn add ember-validated-form
ember install ember-concurrency
yarn remove ember-concurrency --dev && yarn add ember-concurrency
ember install ember-intl 
yarn remove ember-intl --dev && yarn add ember-intl
cp -r ../../../templates/ui/monorepo-components/translations ./
ember g component ember-validated-form-components/button
ember g component ember-validated-form-components/input
ember g component ember-validated-form-components/label
ember g component ember-validated-form-components/render
ember g component ember-validated-form-components/error
cp ../../../templates/ui/monorepo-components/config/environment.js ./config/
cp -r ../../../templates/ui/monorepo-components/addon/validations ./addon/
cp -r ../../../templates/ui/monorepo-components/addon/components/ember-validated-form-components ./addon/components/
popd

# Ember-simple-auth
pushd ui/frontend
ember install ember-simple-auth
ember g authenticator devise-token-auth --base-class=devise
# TODO: waiting for: https://github.com/simplabs/ember-simple-auth/issues/2206
cp ../../../templates/ui/frontend/app/authenticators/devise-token-auth.js ./app/authenticators/
ember g adapter application
cp ../../../templates/ui/frontend/app/adapters/application.js ./app/adapters
cp ../../../templates/ui/frontend/app/routes/application.js ./app/routes
ember g route authenticated
cp ../../../templates/ui/frontend/app/templates/authenticated.hbs ./app/templates
sed -i '' -e '/locationType/r ../../../templates/ui/frontend/config/environment.js_part_01' ./config/environment.js
popd

# Devise
pushd backend
bundle add devise bcrypt
rails generate devise:install
bundle add devise_token_auth
rails generate devise_token_auth:install User users
sed -i '' -e '/config.change_headers_on_each_request/r ../../templates/backend/config/initializers/devise_token_auth.rb_part_01' ./config/initializers/devise_token_auth.rb
# TODO: https://github.com/lynndylanhurley/devise_token_auth/issues/1356#issuecomment-636359066
sed -i '' -e 's/:trackable, //g' app/models/user.rb
rails generate controller users
cp ../../templates/backend/app/controllers/users_controller.rb ./app/controllers
rails db:migrate
popd

# JSONAPI-resources
pushd backend
# TOOD: https://github.com/venuu/jsonapi-authorization/issues/64
bundle add jsonapi-resources --version='~>0.9.0'
rails g controller api
cp ../../templates/backend/app/controllers/api_controller.rb ./app/controllers
bundle add rack-cors
sed -i '' -e '/Rails::Application/r ../../templates/backend/config/application.rb_part_01' ./config/application.rb
sed -i '' -e '/Rails.application.routes.draw/r ../../templates/backend/config/routes.rb_part_01' ./config/routes.rb
popd

# Pundit
pushd backend
bundle add pundit
rails g pundit:install
bundle add jsonapi-authorization
cp ../../templates/backend/config/initializers/jsonapi_resources.rb ./config/initializers/
cp ../../templates/backend/config/initializers/jsonapi_authorization.rb ./config/initializers/
popd

# Organization
pushd backend
rails g model Organization name:string
rails g migration AddOrganizationToUsers organization:references
sed -i '' -e 's/ActiveRecord::Base/OrganizationDependent/' ./app/models/user.rb
mkdir -p ./app/resources
mkdir -p ./app/resources/api
mkdir -p ./app/controllers/api
cp ../../templates/backend/app/models/organization_dependent.rb ./app/models/
cp ../../templates/backend/app/policies/application_policy.rb ./app/policies/
cp ../../templates/backend/app/policies/organization_dependent_policy.rb ./app/policies/
cp ../../templates/backend/app/resources/api/base_resource.rb ./app/resources/api/
cp ../../templates/backend/app/resources/api/organization_dependent_resource.rb ./app/resources/api/
cp ../../templates/backend/app/resources/api/organization_resource.rb ./app/resources/api/
rails db:migrate
popd

# Demo resource - poll
pushd backend
rails g model Poll name:string organization:references
rails g model Poll name:string organization:references --parent=OrganizationDependent --force
rails db:migrate
cp ../../templates/backend/app/policies/poll_policy.rb ./app/policies/
cp ../../templates/backend/app/controllers/api/polls_controller.rb ./app/controllers/api/
cp ../../templates/backend/app/resources/api/poll_resource.rb ./app/resources/api/
sed -i '' -e '/namespace :api/r ../../templates/backend/config/routes.rb_part_02' ./config/routes.rb
popd
pushd ui/frontend
ember g model poll name:string
ember g route authenticated/polls
cp ../../../templates/ui/frontend/app/routes/authenticated/polls.js ./app/routes/authenticated/
cp ../../../templates/ui/frontend/app/templates/authenticated/polls.hbs ./app/templates/authenticated/
popd

# Demo organization
pushd backend
rails runner "Organization.create(name: 'Test organization')"
popd

# Demo user
pushd backend
rails runner "User.create(email: 'demo@example.com', password: 'demo@example.com', organization: Organization.first)"
popd

# Demo polls
pushd backend
rails runner "Poll.create(name: 'test 1', organization: Organization.first)"
rails runner "Poll.create(name: 'test 2', organization: Organization.first)"
rails runner "Poll.create(name: 'test 3', organization: Organization.first)"
popd

# Formatting
pushd ui/frontend
yarn add -D ember-template-lint
yarn add -D prettier ember-template-lint-plugin-prettier
yarn add -D ember-template-lint-plugin-tailwindcss
cp ../../../templates/ui/frontend/.template-lintrc.js ./
popd
pushd ui/monorepo-components
yarn add -D ember-template-lint
yarn add -D prettier ember-template-lint-plugin-prettier
yarn add -D ember-template-lint-plugin-tailwindcss
cp ../../../templates/ui/monorepo-components/.template-lintrc.js ./
popd

# ember-cli-deploy
pushd ui/frontend
ember install ember-cli-deploy
ember install ember-cli-deploy-build
popd

# Netlify
pushd ui/frontend
ember install ember-cli-netlify
ember install ember-cli-deploy-netlify-cli
cp ../../../templates/ui/frontend/public/_redirects ./public/
sed -i '' -e '/build:/r ../../../templates/ui/frontend/config/deploy.js_part_01' ./config/deploy.js
popd

# Gitlab-CI
cp ../templates/.gitlab-ci.yml ./

# Icons
pushd ui/frontend
ember install @fortawesome/ember-fontawesome
yarn add --dev @fortawesome/free-solid-svg-icons

# Build docker image
docker-compose down
docker-compose build
docker-compose run --rm backend rails db:migrate

# Save game
git add .
git commit -m 'Initial commit'

# Thank you
echo "🎆 The app is ready 🎇"
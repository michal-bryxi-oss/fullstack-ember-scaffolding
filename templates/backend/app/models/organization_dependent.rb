# frozen_string_literal: true

class OrganizationDependent < ApplicationRecord
  self.abstract_class = true

  belongs_to :organization
end

# frozen_string_literal: true

module Api
  class PollResource < Api::OrganizationDependentResource
    model_name 'Poll'

    attribute :name
  end
end

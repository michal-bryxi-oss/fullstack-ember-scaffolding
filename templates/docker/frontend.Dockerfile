FROM debian:stable-slim

RUN apt-get update \
  && apt-get install -y \
  git \
  curl \
  openssh-client \
  ca-certificates \
  chromium \
  --no-install-recommends

ENV APP frontend
ENV BASH_ENV ~/.bashrc
ENV VOLTA_HOME /root/.volta
ENV PATH $VOLTA_HOME/bin:$PATH

SHELL ["/bin/bash", "-c"]
WORKDIR /myapp/$APP

RUN mkdir /myapp/monorepo-components
COPY ui/package.json ui/yarn.lock /myapp/
COPY ui/monorepo-components/package.json /myapp/monorepo-components/
COPY ui/$APP/package.json /myapp/$APP/

RUN curl https://get.volta.sh | bash
RUN env
RUN volta --version
RUN node --version
RUN pwd
RUN volta install ember-cli netlify-cli
RUN yarn --pure-lockfile
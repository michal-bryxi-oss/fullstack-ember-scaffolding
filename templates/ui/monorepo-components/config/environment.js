'use strict';

module.exports = function(/* environment, appConfig */) {
  return {
    "ember-validated-form": {
      defaults: {
        error: "ember-validated-form-components/error",
        // hint: "x-my-hint",
        label: "ember-validated-form-components/label",
        render: "ember-validated-form-components/render",

        // button
        button: "ember-validated-form-components/button",

        // types
        // "types/checkbox": "x-my-checkbox",
        "types/input": "ember-validated-form-components/input",
        // "types/radio-group": "x-my-radio-group",
        // "types/select": "x-my-select",
        // "types/textarea": "x-my-textarea"
      }
    }
  };
};

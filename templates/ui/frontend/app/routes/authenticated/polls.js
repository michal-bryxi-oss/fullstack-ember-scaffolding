import Route from '@ember/routing/route';

export default class AuthenticatedPollsRoute extends Route {
  model() {
    return this.store.findAll('poll');
  }
}
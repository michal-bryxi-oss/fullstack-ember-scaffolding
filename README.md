# Fullstack Ember Scaffolding

## Goal

Starting a new full-stack project can be a wee bit of a daunting task
as there are so many parts that need to be wired and set-up:
 
 - Frontend app(s)
 - Common components
 - CSS framework
 - Authentication
 - Backend
 - Database
 - CI
 - Linters
 - Deployment
 - Testing

... just to name few.

This script is intended to simplify that process.
At the moment it's tailored for my (@MichalBryxi) favourite stack.

It will be always based on [EmberJS](https://emberjs.com/)
and the subsequent technologies can vary.

## Stack

- [x] [EmberJS](https://emberjs.com/)
- [x] [volta](https://volta.sh/)
- [x] [yarn](https://yarnpkg.com/)
- [x] [ember-changeset](https://github.com/poteto/ember-changeset)
- [x] [ember-changeset-validations](https://github.com/poteto/ember-changeset-validations)
- [x] [ember-validated-form](https://adfinis-sygroup.github.io/ember-validated-form/)
- [x] [ember-concurrency](http://ember-concurrency.com/docs/introduction/)
- [x] [ember-intl](https://ember-intl.github.io/ember-intl/)
- [x] [ember-simple-auth](https://ember-simple-auth.com/)
- [ ] eslint
- [ ] prettier

- [x] [Ruby on Rails](https://rubyonrails.org/)
- [x] [rbenv](https://github.com/rbenv/rbenv)
- [x] [devise](https://github.com/heartcombo/devise)
- [x] [devise-token-auth](https://devise-token-auth.gitbook.io/devise-token-auth/)
- [ ] [rubocop](https://github.com/rubocop-hq/rubocop)
- [ ] JSONAPI-resources

- [x] [PostgreSQL](https://www.postgresql.org/)

- [x] [TailwindCSS])(https://tailwindcss.com/) 

- [x] [Docker](https://www.docker.com/)
- [ ] [GitLab CI](https://docs.gitlab.com/ee/ci/)
- [ ] [Netlify](https://www.netlify.com/)
- [ ] [Heroku](https://www.heroku.com/)

## Requirements

- [rbenv](https://github.com/rbenv/rbenv)
- [rails](https://rubyonrails.org/)
- [postgresql](https://formulae.brew.sh/formula/postgresql#default)

- [docker-compose](https://docs.docker.com/compose/)

- [volta](https://volta.sh/)

## Usage

Following command will scaffold the app for you into a folder called `./scaffolding`:
```sh
./script.sh
```

After that you can run the backend as follows:
```sh
cd scaffolding;
docker-compose up
```

In another terminal window you can run the frontend:
```sh
cd scaffolding/ui/frontend
yarn start
```

Now you can try to log in with username: `demo@example.com` and password: `demo@example.com`.

![Screenshot](./docs/screenshot.png)